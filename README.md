# Патч для ядра Linux на сенсор digma eve 10 c412t

1. Файл gsl1680-digma_es1241ew.fw (прошивка) поместить в папку /lib/firmware/silead/

2. es1241ew_touchscreen.patch (Патч) в место сборки 

3. После установки надо инвертировать акселерометра (См. ссылки)

## Сборка для fedora

1. Идём сюда и качаем исходники https://koji.fedoraproject.org/koji/packageinfo?packageID=8

2. Устанавливаем их командой rpm -Uvh <пакет>

3. В папку ~/rpmbuild/SOURCES закидываем es1241ew_touchscreen.patch

4. Переходим ~/rpmbuild/SPECS/kernel.spec и редактируем.

5. Заменяем # define buildid .local на %define buildid .digma (.digma можете заменить на то что больше нравиться)

6. После Patch1: patch-%{patchversion}-redhat.patch вставляем
Patch2: es1241ew_touchscreen.patch (Объявляем патча)


7. После ApplyOptionalPatch patch-%{patchversion}-redhat.patch вставляем
ApplyOptionalPatch es1241ew_touchscreen.patch (Применяем патч)

8. В папке ~/rpmbuild/SPECS/ применяем команду на сборку 
rpmbuild -ba --without debug --without doc --without perf --without tools --without debuginfo --without kdump --without cross_headers ~/rpmbuild/SPECS/kernel.spec

9. Для нормальной работы акселерометра, файл 61-sensor-local закинуть в папку /etc/udev/hwdb.d/
И выполнить две команды: systemd-hwdb update и udevadm trigger -v -p DEVNAME=/dev/iio:deviceXXX (XXX - id сенсора в моем случае 0)

## Полезные ссылки

1. То как надо настроить сенсор, чтобы экран поворачивался правильно https://github.com/systemd/systemd/blob/main/hwdb.d/60-sensor.hwdb

2. О том как извлечь прошивку и подобрать нужные параметры для вашего устройства https://github.com/onitake/gsl-firmware
